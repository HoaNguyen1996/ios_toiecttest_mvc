//
//  DocumentModel.swift
//  ToiecTestApp
//
//  Created by Apple on 2/16/21.
//

import Foundation

enum DocumentType {
	case newFormat
	
	static func getDocumnentType(documentType: DocumentType) -> String {
		switch documentType {
			case .newFormat:
				return Strings.DOCUMENT_NEW_FORMAT
			default:
				return ""
		}
	}
}

enum DocumentDownloadStatus {
	case unfinished //
	case finished
}

class DocumentModel {
	var title: String
	var questionNumber: Int
	var size: Float
	var unit: String
	var type: DocumentType
	var downloadStatus:DocumentDownloadStatus
	
	init(title:String, questionNumber: Int, size: Float, unit: String, type: DocumentType, downloadStatus: DocumentDownloadStatus) {
		self.title = title
		self.questionNumber = questionNumber
		self.size = size
		self.unit = unit
		self.type = type
		self.downloadStatus = downloadStatus
	}
}

extension DocumentModel{
	static func getDocuments(completion: @escaping([DocumentModel]) -> Void) {
		DispatchQueue.global(qos: .userInitiated).async {
			let documents = [
				DocumentModel(title: "Photograph 01", questionNumber: 6, size: 1.2, unit: "MB", type: DocumentType.newFormat, downloadStatus: DocumentDownloadStatus.unfinished),
				DocumentModel(title: "Photograph 02", questionNumber: 6, size: 1.2, unit: "MB", type: DocumentType.newFormat, downloadStatus: DocumentDownloadStatus.finished),
			]
			
			DispatchQueue.main.async {
				completion(documents)
			}
		}
		
		
	}
}
