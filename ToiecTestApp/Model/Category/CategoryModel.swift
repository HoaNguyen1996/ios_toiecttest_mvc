//
//  CategoryModel.swift
//  ToiecTestApp
//
//  Created by Apple on 2/14/21.
//

import Foundation
import UIKit

class CategoryModel {
	var icon: UIImage
	var title: String
	var description: String
	
	init(icon: UIImage, title: String, description: String) {
		self.icon = icon
		self.title = title
		self.description = description
	}
}

extension CategoryModel{
	static func getCategoryHeader() -> [String] {
		return [Strings.HEADER_PRACTISE_LISTENING, Strings.HEADER_PRACTISE_READING, Strings.HEADER_PRACTISE_TEST]
	}
	
	static func getListeningCategoryModels() -> [CategoryModel]{
		let listeningCates : [CategoryModel] = [
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART1, description: Strings.DESCRIPTION_PHOTOGRAPH),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART2, description: Strings.DESCRIPTION_QUESTION_AND_RESPONSE),
			CategoryModel(icon:UIImage(systemName: "waveform")!, title: Strings.TITLE_PART3, description: Strings.DESCRIPTION_CONVERSATION),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART4, description: Strings.DESCRIPTION_SHORT_TALK),
		]
		return listeningCates
	}
	
	static func getReadingCategoryModels() -> [CategoryModel]{
		let readingCates : [CategoryModel] = [
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART5, description: Strings.DESCRIPTION_INCOMPLETE_SENTENCES),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART6, description: Strings.DESCRIPTION_TEXT_COMPLETION),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_PART7, description: Strings.DESCRIPTION_READING_COMPREHENSION),
			
		]
		return readingCates
	}
	
	static func getTestCategoryModels() -> [CategoryModel]{
		let testCates : [CategoryModel] = [
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_LISTENING_TEST, description: Strings.DESCRIPTION_FULLPART_LISTENING),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_READING_TEST, description: Strings.DESCRIPTION_FULLPART_READING),
			CategoryModel(icon: UIImage(systemName: "waveform")!, title: Strings.TITLE_FULL_TEST, description: Strings.DESCRIPTION_FULLTEST),
			
		]
		return testCates
	}
}
