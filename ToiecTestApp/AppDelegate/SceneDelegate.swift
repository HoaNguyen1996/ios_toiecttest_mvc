//
//  SceneDelegate.swift
//  ToiecTestApp
//
//  Created by Apple on 2/12/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?


	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		// Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
		// If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
		// This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
		guard let windowScene = (scene as? UIWindowScene) else { return }
		let window = UIWindow(windowScene: windowScene)
		// Practis
		let practiseVC = PractiseViewController()
		let practiseNV = UINavigationController(rootViewController: practiseVC)
		practiseNV.navigationBar.topItem?.title = Strings.TAB_PRACTISE
		practiseNV.tabBarItem = UITabBarItem(title: Strings.TAB_PRACTISE, image: UIImage(systemName: Svgs.IC_UNTAB_PRACTISE), selectedImage: UIImage(systemName: Svgs.IC_TAB_PRACTISE))
		
		// Favourite
		let favouriteVC = FavouriteViewController()
		let favouriteNV = UINavigationController(rootViewController: favouriteVC)
		favouriteNV.navigationBar.topItem?.title = Strings.TAB_FAVOURITE
		favouriteNV.tabBarItem = UITabBarItem(title: Strings.TAB_FAVOURITE, image: UIImage(systemName: Svgs.IC_UNTAB_FAVOURITE), selectedImage: UIImage(systemName: Svgs.IC_TAB_FAVOURITE))
		
		// Statistical
		let statisticVC = StatisticViewController()
		let statisticNV = UINavigationController(rootViewController: statisticVC)
		statisticNV.navigationBar.topItem?.title = Strings.TAB_STATISTIC
		statisticNV.tabBarItem = UITabBarItem(title: Strings.TAB_STATISTIC, image: UIImage(systemName: Svgs.IC_UNTAB_STATISTIC), selectedImage: UIImage(systemName: Svgs.IC_TAB_STATISTIC))
		
		// Download
		let downloadVC = DownloadViewController()
		let downloadNV = UINavigationController(rootViewController: downloadVC)
		downloadNV.navigationBar.topItem?.title = Strings.TAB_DOWNLOAD
		downloadNV.tabBarItem = UITabBarItem(title: Strings.TAB_DOWNLOAD, image: UIImage(systemName: Svgs.IC_UNTAB_DONWLOAD), selectedImage: UIImage(systemName: Svgs.IC_TAB_DOWNLOAD))
		
		// Settings
		let settingVC = SettingsViewController()
		let settingNV = UINavigationController(rootViewController: settingVC)
		settingNV.navigationBar.topItem?.title = Strings.TAB_SETTING
		settingNV.tabBarItem = UITabBarItem(title: Strings.TAB_SETTING, image: UIImage(systemName: Svgs.IC_TAB_SETTINGS), selectedImage: UIImage(systemName: Svgs.IC_TAB_SETTINGS))
		
		self.window = window
		window.makeKeyAndVisible()
		
		// Create tabbar
		let tabbarViewControllers:[UINavigationController] = [practiseNV, favouriteNV, statisticNV, downloadNV, settingNV]
		let tabbarController = UITabBarController()
		tabbarController.viewControllers = tabbarViewControllers
		tabbarController.tabBar.tintColor = Colors.mainColor
		
		// Set tabbar display in the first time
		window.rootViewController = tabbarController
	}

	func sceneDidDisconnect(_ scene: UIScene) {
		// Called as the scene is being released by the system.
		// This occurs shortly after the scene enters the background, or when its session is discarded.
		// Release any resources associated with this scene that can be re-created the next time the scene connects.
		// The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
	}

	func sceneDidBecomeActive(_ scene: UIScene) {
		// Called when the scene has moved from an inactive state to an active state.
		// Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
	}

	func sceneWillResignActive(_ scene: UIScene) {
		// Called when the scene will move from an active state to an inactive state.
		// This may occur due to temporary interruptions (ex. an incoming phone call).
	}

	func sceneWillEnterForeground(_ scene: UIScene) {
		// Called as the scene transitions from the background to the foreground.
		// Use this method to undo the changes made on entering the background.
	}

	func sceneDidEnterBackground(_ scene: UIScene) {
		// Called as the scene transitions from the foreground to the background.
		// Use this method to save data, release shared resources, and store enough scene-specific state information
		// to restore the scene back to its current state.
	}


}

