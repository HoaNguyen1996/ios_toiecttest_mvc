//
//  Strings.swift
//  ToiecTestApp
//
//  Created by Apple on 2/12/21.
//

import Foundation
import UIKit

struct Strings {
	// MARK - <#Tab Strings
	static let TAB_PRACTISE = getLocalizeStrings(key:"tab.practise")
	static let TAB_FAVOURITE = getLocalizeStrings(key:"tab.favourite")
	static let TAB_STATISTIC = getLocalizeStrings(key:"tab.statistic")
	static let TAB_DOWNLOAD = getLocalizeStrings(key:"tab.download")
	static let TAB_SETTING = getLocalizeStrings(key:"tab.settings")
	
	static let HEADER_PRACTISE_LISTENING = getLocalizeStrings(key:"header.pratise.listening")
	static let HEADER_PRACTISE_READING = getLocalizeStrings(key:"header.practise.reading")
	static let HEADER_PRACTISE_TEST = getLocalizeStrings(key:"header.practise.test")
	
	static let TITLE_PART1 = getLocalizeStrings(key:"title.part1")
	static let TITLE_PART2 = getLocalizeStrings(key:"title.part2")
	static let TITLE_PART3 = getLocalizeStrings(key:"title.part3")
	static let TITLE_PART4 = getLocalizeStrings(key:"tilte.part4")
	static let TITLE_PART5 = getLocalizeStrings(key:"title.part5")
	static let TITLE_PART6 = getLocalizeStrings(key:"title.part6")
	static let TITLE_PART7 = getLocalizeStrings(key:"title.part7")
	static let TITLE_LISTENING_TEST = getLocalizeStrings(key:"title.listeningTest")
	static let TITLE_READING_TEST = getLocalizeStrings(key:"title.readingTest")
	static let TITLE_FULL_TEST = getLocalizeStrings(key:"title.fullTest")
	
	static let DESCRIPTION_PHOTOGRAPH = getLocalizeStrings(key:"description.photographs")
	static let DESCRIPTION_QUESTION_AND_RESPONSE = getLocalizeStrings(key:"description.questionAndResponse")
	static let DESCRIPTION_CONVERSATION = getLocalizeStrings(key:"description.conversations")
	static let DESCRIPTION_SHORT_TALK = getLocalizeStrings(key:"description.shortTalk")
	static let DESCRIPTION_INCOMPLETE_SENTENCES = getLocalizeStrings(key:"description.incompleteSentences")
	static let DESCRIPTION_TEXT_COMPLETION = getLocalizeStrings(key:"description.textCompletion")
	static let DESCRIPTION_READING_COMPREHENSION = getLocalizeStrings(key:"description.readingComprehension")
	static let DESCRIPTION_FULLPART_LISTENING = getLocalizeStrings(key:"description.fullPartListening")
	static let DESCRIPTION_FULLPART_READING = getLocalizeStrings(key:"description.fullpartReading")
	static let DESCRIPTION_FULLTEST = getLocalizeStrings(key:"description.fullTest")
	
	static let DOCUMENT_QUESTION_NUMBER = getLocalizeStrings(key: "document.questionNumber")
	static let DOCUMENT_SIZE = getLocalizeStrings(key: "document.size")
	static let DOCUMENT_NEW_FORMAT = getLocalizeStrings(key: "document.type.newFormat")
	
}

extension Strings {
	static func getLocalizeStrings(key: String) -> String{
		return NSLocalizedString(key, comment: "")
	}
}
