//
//  Images.swift
//  ToiecTestApp
//
//  Created by Apple on 2/12/21.
//

import Foundation

struct Svgs {
	static let IC_TAB_PRACTISE = "house.fill"
	static let IC_UNTAB_PRACTISE = "house"
	
	static let IC_TAB_FAVOURITE = "heart.fill"
	static let IC_UNTAB_FAVOURITE = "heart"
	
	static let IC_TAB_STATISTIC = "chart.pie.fill"
	static let IC_UNTAB_STATISTIC = "chart.pie"
	
	static let IC_TAB_DOWNLOAD = "square.and.arrow.down.on.square.fill"
	static let IC_UNTAB_DONWLOAD = "square.and.arrow.down.on.square"
	
	static let IC_TAB_SETTINGS = "gear"
	
	static let IC_DOWNLOAD = "square.and.arrow.down"
	static let IC_FINISH = "checkmark"
}
