//
//  Colors.swift
//  ToiecTestApp
//
//  Created by Apple on 2/12/21.
//
import UIKit
import Foundation

struct Colors {
	static let mainColor:UIColor = getFromHexColors(hex:"#00e3b8")
	static let bgColor: UIColor = UIColor(named: "bgColor")!
	static let bgItem: UIColor = UIColor(named: "bgItem")!
	static let listeningColor: UIColor = getFromHexColors(hex: "#8e60f2")
	static let readingColor:UIColor = getFromHexColors(hex: "#44bdaa")
	static let testColor: UIColor = getFromHexColors(hex: "#025dae")
	
	static func getFromHexColors(hex: String, alpha:CGFloat  = 1.0) -> UIColor{
		var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
		
		if (cString.hasPrefix("#")) {
			cString.remove(at: cString.startIndex)
		}
		
		if ((cString.count) != 6) {
			return UIColor.gray
		}
		
		var rgbValue:UInt64 = 0
		Scanner(string: cString).scanHexInt64(&rgbValue)
		
		return UIColor(
			red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
			alpha: CGFloat(alpha)
		)
	}
	
	static func getFromHexColors(rgb: Int, alpha:CGFloat  = 1.0) -> UIColor{
		return UIColor(red: CGFloat((rgb >> 16) & 0xFF),
						green: CGFloat((rgb >> 8) & 0xFF),
						blue: CGFloat(rgb & 0xFF),
						alpha: alpha)
	}
	
	static func getFromHexColors(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) -> UIColor{
		return UIColor(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: alpha)
	}
}
