//
//  CellIdentifier.swift
//  ToiecTestApp
//
//  Created by Apple on 2/15/21.
//

import Foundation

struct CellIdentifier {
	static let PractiseHeaderReusableView = "PractiseHeaderView"
	static let PractiseHeader = "PractiseHeader" // cell reuse
	static let PractiseViewCell = "PractiseViewCell" //  class name
	static let PractiseCell = "PractiseCell" // cell reuse
	
	static let DocumentCell = "DocumentCell"
}
