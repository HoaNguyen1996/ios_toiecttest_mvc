//
//  PractiseViewController.swift
//  ToiecTestApp
//
//  Created by Apple on 2/12/21.
//

import UIKit
import SnapKit

class PractiseViewController: UIViewController {
	var cateHeader: [String] = []
	var listeningCates: [CategoryModel] = []
	var readingCates: [CategoryModel] = []
	var testCates : [CategoryModel] = []
	
	
	lazy var categoryCV: UICollectionView = {
		let cv =  UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
		// Setup theme
		cv.backgroundColor = Colors.bgColor
		
		// RegriseHeader
		let headerNib = UINib(nibName: CellIdentifier.PractiseHeaderReusableView, bundle: .main)
		cv.register(headerNib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CellIdentifier.PractiseHeader)

		
		// RegriseCell
		let nib = UINib(nibName: CellIdentifier.PractiseViewCell, bundle: .main)
		cv.register(nib, forCellWithReuseIdentifier: CellIdentifier.PractiseCell)
		//1
		let screenWidth = UIScreen.main.bounds.width - 30
		//2
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 20, right: 10)
		layout.itemSize = CGSize(width: screenWidth/2, height: 125)
		layout.minimumInteritemSpacing = 10
		layout.minimumLineSpacing = 10
		//3
		cv.collectionViewLayout = layout
		
		return cv
	}()
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		configureViews()
        // Do any additional setup after loading the view.
		
    }
	
	func configureViews() -> Void {
		// set collectionsView
		setupCollectionView()
	}
	
	func setupCollectionView() -> Void {
		cateHeader = CategoryModel.getCategoryHeader()
		listeningCates = CategoryModel.getListeningCategoryModels()
		readingCates = CategoryModel.getReadingCategoryModels()
		testCates = CategoryModel.getTestCategoryModels()
		
		view.addSubview(categoryCV)
		categoryCV.snp.makeConstraints { (make) in
			if #available(iOS 11.0, *) {
				make.edges.equalTo(view.safeAreaLayoutGuide)
			} else {
				make.edges.equalTo(view.layoutMargins)
			}
		}
		categoryCV.dataSource = self
		categoryCV.delegate = self
	}
}

/*
// MARK: - Delegate Collection View
}
*/
extension PractiseViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return cateHeader.count
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (section == 0) {return listeningCates.count}
		if (section == 1) {return readingCates.count}
		if (section == 2) {return testCates.count}
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.PractiseCell, for: indexPath) as! PractiseViewCell
		let item:CategoryModel = indexPath.section == 0 ? listeningCates[indexPath.row] : indexPath.section == 1 ? readingCates[indexPath.row] : testCates [indexPath.row]
		// Set cellIcon
		cell.cellIcon.image = item.icon
		let tintColor:UIColor = indexPath.section == 0 ? Colors.listeningColor : indexPath.section == 1 ? Colors.readingColor : Colors.testColor
		cell.setIconTintColor(color: tintColor)
		// Set cellTitle
		cell.cellTitle.text = item.title
		// Set cellDescription
		cell.cellDescription.text = item.description
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return CGSize(width: collectionView.frame.width, height: 50)
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		switch kind {
			case UICollectionView.elementKindSectionHeader:
				let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: CellIdentifier.PractiseHeader, for: indexPath) as! PractiseHeaderView
				
				let header = cateHeader[indexPath.section]
				
				reusableview.headerLabel.text = header
				return reusableview
			default:
				fatalError("Unexpected element kind")
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let detailDocumentViewController = DocumentsViewController()
		var title = ""
		if (indexPath.section == 0) {title = listeningCates[indexPath.item].title}
		if (indexPath.section == 1) {title = readingCates[indexPath.item].title}
		if (indexPath.section == 2) {title = testCates[indexPath.item].title}
		detailDocumentViewController.title = title
		navigationController?.pushViewController(detailDocumentViewController, animated: true)
	}
	
}
