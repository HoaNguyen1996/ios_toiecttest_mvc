//
//  PractiseViewCell.swift
//  ToiecTestApp
//
//  Created by Apple on 2/15/21.
//

import UIKit

class PractiseViewCell: UICollectionViewCell {

	@IBOutlet weak var cellIcon: UIImageView!
	@IBOutlet weak var cellTitle: UILabel!
	@IBOutlet weak var cellDescription: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		configureViews()
    }
	
	func configureViews() -> Void{
		self.contentView.dropShawdow()
		self.contentView.cornerRadius()
		self.contentView.backgroundColor = Colors.bgItem
	}
	
	func setIconTintColor(color: UIColor)-> Void{
		self.cellIcon.tintColor = color
	}

}
