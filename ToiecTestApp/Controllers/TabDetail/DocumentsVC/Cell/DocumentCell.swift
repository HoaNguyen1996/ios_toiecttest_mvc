//
//  DocumentCell.swift
//  ToiecTestApp
//
//  Created by Apple on 2/16/21.
//

import UIKit

class DocumentCell: UICollectionViewCell {


	@IBOutlet weak var containerSv: UIStackView!
	@IBOutlet weak var titleLb: UILabel!
	@IBOutlet weak var qsNumberLb: UILabel!
	@IBOutlet weak var sizeLb: UILabel!
	@IBOutlet weak var typeLb: PaddingLabel!
	
	@IBOutlet weak var downloadBtnView: UIStackView!
	@IBOutlet weak var downloadBtn: UIButton!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		configureViews()
    }

    
	func configureViews() -> Void {
		self.contentView.backgroundColor = .clear
		
		containerSv.backgroundColor = Colors.bgItem
		containerSv.cornerRadius()
		
		typeLb.padding(5, 5, 5, 5)
		typeLb.layer.cornerRadius = 8
		typeLb.layer.masksToBounds = true
		typeLb.backgroundColor = Colors.mainColor
		
		downloadBtnView.cornerRadius(cornerRadius: 16, borderColor: Colors.mainColor, borderWidth: 1)
		downloadBtn.imageView?.tintColor = Colors.mainColor
	}
	
	func setup(documentModel: DocumentModel) -> Void {
		titleLb.text = documentModel.title
		qsNumberLb.text = "\(Strings.DOCUMENT_QUESTION_NUMBER): \(documentModel.questionNumber)"
		qsNumberLb.setColorSpecificText(color: Colors.mainColor, forText: "\(documentModel.questionNumber)")
		sizeLb.text = " - \(Strings.DOCUMENT_SIZE): \(documentModel.size) \(documentModel.unit)"
		sizeLb.setColorSpecificText(color: Colors.mainColor, forText: "\(documentModel.size) \(documentModel.unit)")
		typeLb.text = DocumentType.getDocumnentType(documentType: documentModel.type).uppercased()
		setIconBtn(documentDownloadStatus: documentModel.downloadStatus)
	}
	
	func setIconBtn(documentDownloadStatus: DocumentDownloadStatus) -> Void {
		let btnIcon = documentDownloadStatus == DocumentDownloadStatus.unfinished ? UIImage(systemName: Svgs.IC_DOWNLOAD) : UIImage(systemName: Svgs.IC_FINISH)
		downloadBtn.setImage(btnIcon, for: .normal)
		
	}
    
}
