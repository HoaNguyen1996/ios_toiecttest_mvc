//
//  DocumentsViewController.swift
//  ToiecTestApp
//
//  Created by Apple on 2/16/21.
//

import UIKit

class DocumentsViewController: UIViewController {
	var documents = [DocumentModel](){
		didSet {
			documentCollectionView.reloadData()
		}
	}
	
	lazy var documentCollectionView:UICollectionView = {
		let cv =  UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
		// Setup theme
		cv.backgroundColor = Colors.bgColor
		let nib = UINib(nibName: CellIdentifier.DocumentCell, bundle: .main)
		cv.register(nib, forCellWithReuseIdentifier: CellIdentifier.DocumentCell)
		
		//1
		let screenWidth = UIScreen.main.bounds.width - 10
		//2
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
		layout.itemSize = CGSize(width: screenWidth, height: 125)
		layout.minimumInteritemSpacing = 0
		layout.minimumLineSpacing = 5
		//3
		cv.collectionViewLayout = layout
		
		return cv
	}()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		configureViews()
    }
	
	func configureViews(){
		setupTableView()
	}
	
	func setupTableView() -> Void {
		self.view.addSubview(documentCollectionView)
		documentCollectionView.snp.makeConstraints { (make) in
			if #available(iOS 11.0, *) {
				make.edges.equalTo(view.safeAreaLayoutGuide)
			} else {
				make.edges.equalTo(view.layoutMargins)
			}
		}
		
		documentCollectionView.dataSource = self
		documentCollectionView.delegate = self
		DocumentModel.getDocuments { [weak self] (documents) in
			guard let self = self else {return}
			self.documents = documents
		}
	}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DocumentsViewController: UICollectionViewDelegate, UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return documents.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.DocumentCell, for: indexPath) as! DocumentCell
		cell.setup(documentModel: documents[indexPath.item])
		return cell
	}
}
