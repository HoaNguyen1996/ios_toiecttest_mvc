//
//  UILabel+Ext.swift
//  ToiecTestApp
//
//  Created by Apple on 2/16/21.
//

import Foundation
import UIKit

class PaddingLabel: UILabel {
	
	var insets = UIEdgeInsets.zero
	
	func padding(_ top: CGFloat, _ bottom: CGFloat, _ left: CGFloat, _ right: CGFloat) {
		self.frame = CGRect(x: 0, y: 0, width: self.frame.width + left + right, height: self.frame.height + top + bottom)
		insets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
	}
	
	override func drawText(in rect: CGRect) {
		super.drawText(in: rect.inset(by: insets))
	}
	
	override var intrinsicContentSize: CGSize {
		get {
			var contentSize = super.intrinsicContentSize
			contentSize.height += insets.top + insets.bottom
			contentSize.width += insets.left + insets.right
			return contentSize
		}
	}
	
}

extension NSMutableAttributedString {
	
	func setColor(color: UIColor, forText stringValue: String) {
		let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
		self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
	}
	
}

extension UILabel {
	func setColorSpecificText(color: UIColor, forText stringValue: String) -> Void {
		let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
		attributedString.setAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : color], range: (self.text! as NSString).range(of: stringValue))
		self.attributedText = attributedString
	}
}


