//
//  UIView+Ext.swift
//  ToeicTest
//
//  Created by Duc Minh Nguyen on 1/18/21.
//

import UIKit

extension UIView {
    func dropShawdow(shadowRadius: CGFloat = 8,
                     color: CGColor = UIColor.black.withAlphaComponent(0.18).cgColor,
                     offSet: CGSize = CGSize(width: 2, height: 2)) {
        clipsToBounds = false
        layer.masksToBounds = false
		
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = 1
        layer.shadowColor = color
        layer.shadowOffset = offSet
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
	
	func cornerRadius(cornerRadius: CGFloat = 8, borderColor:UIColor = .clear, borderWidth: CGFloat = 0) {
		clipsToBounds = false
		layer.masksToBounds = false
		layer.cornerRadius = cornerRadius
		layer.borderWidth = borderWidth
		layer.borderColor = borderColor.cgColor
		
	}
}
